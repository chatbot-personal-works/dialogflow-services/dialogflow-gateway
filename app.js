const express=require("express")
const app=express()
const path=require("path")
const dialog=require("./x.js")
const cors = require('cors');

app.use(cors())

app.use(express.static("static"))
app.use(express.json());
const port=5005



app.get("/",(req,res)=>{
	res.send("Server is up!!")})

app.post("/chatbot/:session",async (req,res)=>{
	response=await dialog.executeQueries(req.params.session, req.body.queryInput.text.text)
	res.send(JSON.stringify(response))
})

app.get("/agent",async (req,res)=>{
	response=await dialog.getAgent()
	res.send(JSON.stringify(response[0]))
})

app.listen(port,"0.0.0.0",()=>console.log("Server is running on port: ",port))
